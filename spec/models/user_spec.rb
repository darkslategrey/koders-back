require 'spec_helper'


describe User, :type => :model do

  it "should have a factory" do
    expect(FactoryGirl.build(:user)).to be_valid
  end

  context "validations" do
    it { should validate_uniqueness_of(:email) }
    it { should validate_presence_of(:email) }    
    it { should validate_presence_of(:firstname) }
    it { should validate_presence_of(:lastname) }
  end
  
end


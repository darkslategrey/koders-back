require 'rubygems'

ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'pundit/rspec'
require 'database_cleaner'
require 'capybara'
require 'capybara/rails'
require 'capybara/rspec'
require 'capybara/poltergeist'
require 'devise'

# require "email_spec"

Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.include RSpec::Rails::RequestExampleGroup, type: :request, file_path: /spec\/api/
  # config.include Devise::TestHelpers, type: :request
  # config.include Warden::Test::Helpers
  
  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = false

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  config.mock_with :rspec

  # config.include(EmailSpec::Helpers)
  # config.include(EmailSpec::Matchers)

  config.infer_spec_type_from_file_location!
  
  config.include FactoryGirl::Syntax::Methods


  config.before(:suite) do
    DatabaseCleaner[:mongoid].strategy = :truncation
    DatabaseCleaner[:mongoid].clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

end

FactoryGirl.define do
  factory :user do
    email     { Faker::Internet.email }
    firstname { Faker::Name.first_name }
    lastname  { Faker::Name.last_name }
    password "motdepasse"    
    password_confirmation "motdepasse"

    
    trait :admin do
      role :admin
    end
    factory :admin, traits: [:admin]    
  end
end

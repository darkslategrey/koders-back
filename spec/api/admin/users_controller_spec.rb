require 'spec_helper'
require 'pry-rails'

describe "Admin::UsersController" do
  let(:admin) { FactoryGirl.create(:admin) }
  let(:user) { FactoryGirl.create(:user) }

  before do
    expect(admin.admin?).to be true
    expect(user.user?).to be true
    # request.env["devise.mapping"] = Devise.mappings[:user]    
  end

  describe "Cannot GET 'index' if not admin role" do
    # before do
    #   sign_in @user
    # end
    
    # it "return users list" do
    #   get '/api/admin/users'
    #   expect(response.status).to eq 200
    # end
  end

  # describe "GET 'index' as normal user" do
  #   it "return unauthorized message" do
  #     # 
  #   end
  # end

end

require 'spec_helper'


describe "Auth" do
  let(:user) { FactoryGirl.build(:user) }

  describe "User" do

    before do
      @params = {
        firstname: user.firstname,
        lastname:  user.lastname,
        email:     user.email,
        password:  user.password,
        password_confirmation: user.password_confirmation
      }
      post '/api/auth/sign_up', @params
    end
    
    it "can register" do
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['token']).to be_truthy
    end

    it "cannot register with duplicate email" do
      post '/api/auth/sign_up', @params
      msg = 'Email is already taken'
      expect(JSON.parse(response.body)['message']).to match(/#{msg}/)
    end

    it "can login" do
      post '/api/auth/sign_in', email: user.email, password: user.password
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)["token"]).to be_truthy
      logged_user = User.where(email: user.email).first
      expect(logged_user.sign_in_count).to eq(1)
      expect(logged_user.current_sign_in_at).to be_truthy
      expect(logged_user.current_sign_in_ip).to be_truthy
    end

    it "can logout" do
      post '/api/auth/sign_in', email: user.email, password: user.password
      token = JSON.parse(response.body)["token"]
      delete '/api/auth/sign_out', nil, {'Authorization' => token}
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["result"]).to eq("success")
      logout_user = User.where(email: user.email).first      
      expect(logout_user.current_sign_in_at).to be_falsy
      expect(logout_user.last_sign_in_at).to be_truthy
      expect(logout_user.last_sign_in_ip).to be_truthy      
    end
    
  end

  
end

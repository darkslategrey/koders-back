module Api
  module V1
    class Projects < Grape::API
      include Api::V1::Defaults      

      resource :pojects do
        desc "Return list of recent projects"
        get do
          Project.all
        end

        desc "Return a project"
        params do
          requires :id, type: String, desc: "ID of the project"
        end
        get ":id", root: "project" do
          Project.where(id: permitted_params[:id]).first!
        end
      end
      
    end
  end
end

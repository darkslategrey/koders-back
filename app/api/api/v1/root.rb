require 'grape'
require 'grape-swagger'

module Api
  module V1
    class Root < Grape::API
      include Api::V1::Defaults
      # mount Api::V1::Users
      # mount Api::V1::Projects
      mount Api::V1::Admin
      mount Api::V1::Auth
      
      # formatter :json, Grape::Formatter::Rabl

      add_swagger_documentation(
        api_version: "v1",
        hide_documentation_path: true,
        mount_path: "/api/v1/swagger_doc",
        hide_format: true
      )  


      # helpers do
      #   def get_user
      #     unless @current_user.present?
      #       if params[:api_key].present?
      #         @current_user = User.find_by(api_key: params[:api_key])
      #       end
      #     end

      #     @current_user
      #   end

      #   def authenticate!
      #     error!('401 Unauthorized', 401) unless get_user
      #   end
      # end
      
      helpers do
        def warden
          env['warden']
        end

        def authenticated?
          if warden.authenticated?
            return true
          elsif params[:auth_token] and
               User.find_by_authentication_token(params[:auth_token]) and
               User.find_by_authentication_token(params[:auth_token]).auth_token_expiry > DateTime.now
            return true
          else
            error!({"error" => "Unauth 401. Token invalid or expired"}, 401)
          end
        end

        def current_user
          warden.user ||  User.find_by_authentication_token(params[:auth_token])
        end
      end

      
    end
  end
end

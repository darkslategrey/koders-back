module Api
  module V1
    class Admin < Grape::API
      include Api::V1::Defaults
      prefix "api/admin"

      before do
        error!("401 unauthorized", 401) unless authenticated?
      end

      resource :admins do
        desc "admin list"
        get '/', only: :short do
          authorize! :read, Admin
          paginate(Admin.all)
        end

        desc "create an admin"
        params do
          requires :name,     type: String, desc: "Name"
          requires :email,    type: String, desc: "Email"
          requires :password, type: String, desc: "Password"
        end
        post '/new' do
          authorize! :create, Admin
          admin = Admin.create(permitted_params)
          success(admin)
        end
      end
      
      # resource :users do
      #   desc "Return list of users"
      #   get "", root: :users do
      #     User.all
      #   end

      #   desc "Return a user"
      #   params do
      #     requires :id, type: String, desc: "ID of the User"
      #   end
      #   get ":id", root: "user" do
      #     User.where(id: permitted_params[:id]).first!
      #   end
      # end
    end
  end
end

module Api
  module V1
    class Auth < Grape::API
      resource :auth do
        before_validation { @skip_authen = true }

        ####
        desc "Register a new user"
        params do
          requires :email,     type: String, desc: "User's email"
          requires :firstname, type: String, desc: "User's firstname"
          requires :lastname,  type: String, desc: "User's lastname"
          requires :password,  type: String, desc: "User's password"
          requires :password_confirmation, type: String, desc: "User's password_confirmation"
        end
        post :sign_up do
          user = User.create!(firstname: params[:firstname],
                              lastname:  params[:lastname],
                              password:  params[:password],
                              email:     params[:email],                              
                              password_confirmation: params[:password_confirmation])
          success(user)
        end

        #### 
        desc "Login and generate token"
        params do
          requires :email,    type: String, desc: "Email"
          requires :password, type: String, desc: "Password"
        end
        post :sign_in do
          user = User.where(email: params[:email].downcase).first
          if user and user.valid_password?(params[:password])
            user.sign_in_count += 1
            user.current_sign_in_at = DateTime.now
            user.current_sign_in_ip = env["REMOTE_ADDR"]
            user.save!
            { 
              token: user.authentication_token
            }
          else
            error401
          end
        end

        #### 
        desc "Logout and remove token"
        delete :sign_out do
          if headers['Authorization'] && user = User.where(authentication_token: headers['Authorization']).first
            user.authentication_token = nil
            user.last_sign_in_at      = DateTime.now
            user.current_sign_in_at   = nil
            user.last_sign_in_ip      = env["REMOTE_ADDR"]
            user.save!
            { result: 'success' }
          else
            error401
          end
        end
      end
    end
  end
end

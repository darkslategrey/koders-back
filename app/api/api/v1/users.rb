# coding: utf-8
module Api
  module V1
    class Users < Grape::API
      include Api::V1::Defaults

      resource :users do
        desc "Return list of users"
        get "", root: :users do
          User.all
        end

        ####
        desc "Return a user"
        params do
          requires :id, type: String, desc: "ID of the User"
        end
        get ":id", root: "user" do
          User.where(id: permitted_params[:id]).first!
        end

        ####
        desc "Login a user"
        params do
          requires :email, type: String, desc: "The user's email"
          requires :password, type: String, desc: "The user's password"
        end
        post :sign_in do
          user = User.find_by(email: params[:email])

          if user.present? and user.valid_password?(params[:password])
            { success: true, api_key: user.api_key }
          elsif user.blank?
            { success: false, message: "user not found" }
          else
            { success: false, message: "wrong username or password" }
          end
        end

        ####
        desc "Register a new user"
        params do
          requires :email, type: String, desc: "User's email"
          requires :firstname, type: String, desc: "User's firstname"
          requires :lastname, type: String, desc: "User's lastname"
          requires :password, type: String, desc: "User's password"
          requires :password_confirmation, type: String, desc: "User's password_confirmation"
        end
        post :sign_up do
          user = User.new(name: params[:name],
                          email: params[:email],
                          password: params[:password],
                          password_confirmation: params[:password_confirmation])
          if user.save
            { success: true, api_key: user.api_key }
          else
            { success: false, message: user.errors.full_messages.join(' and ') }
          end
        end

        #### 
        desc "restores a session"
        params do
          requires :api_key, type: String, desc: "user' API key, stored in a local client's database"
        end
        post :restore_session do
          user = User.find_by(api_key: params[:api_key])
          if user.present?
            { success: true }
          else
            { success: false }
          end
        end
      end
    end
  end
end

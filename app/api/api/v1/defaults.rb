module Api
  module V1
    module Defaults
      extend ActiveSupport::Concern

      included do
        prefix "api"
        version 'v1', using: :header, vendor: 'darkslategrey'        
        default_format :json
        format :json

        # formatter :json, Grape::Formatter::ActiveModelSerializers

        helpers do
          def permitted_params
            @permitted_params ||= declared(params, include_missing: false)
          end

          def logger
            Rails.logger
          end

          def success(object=nil)
            # status 200 # uncomment to override, default of create is 201
            if object == nil
              { result: 'success' }
            else
              { id: object.id, token: object.authentication_token }
            end
          end

          def error400 # invalid param
            error!('400 Bad Request', 400)
          end

          def error401 # unauthorized
            error!('401 Unauthorized', 401)
          end

          def error404 # invalid route
            error!('404 Not Found', 404)
          end

          def error422 # before_* callback, or validation fail
            error!('422 Unprocessable Entity', 422)
          end

          def error500 # database error, server error
            error!('500 Internal Server Error', 500)
          end
        end

        rescue_from :all do |e|
          Rails.logger.error "#{e.message}\n\n#{e.backtrace.join("\n")}"
          Rack::Response.new({ message: e.message, backtrace: e.backtrace }.to_json, 500, { 'Content-type' => 'application/json' }).finish
        end
        
        # Rescue_from ActiveRecord::RecordNotFound do |e|
        #   error_response(message: e.message, status: 404)
        # end

        # rescue_from ActiveRecord::RecordInvalid do |e|
        #   error_response(message: e.message, status: 422)
        # end
      end
    end
  end
end

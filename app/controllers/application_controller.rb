class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  # skip_before_action :verify_authentication_token
  protect_from_forgery with: :null_session
end

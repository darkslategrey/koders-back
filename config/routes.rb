
Back::Application.routes.draw do
  devise_for :users
  
  mount Api::Root => '/'
  mount GrapeSwaggerRails::Engine => '/documentation'

  # namespace :api do
  #   resources :tokens, :only => [:create, :destroy]
  # end
  
  # devise_for :users, :path => '/api/users', :path_names => {:sign_in => 'login', :sign_out => 'logout', :sign_up => 'register'}      


  # API Area
  
  # namespace :api, defaults: {format: 'json'}  do
  #   namespace :v1 do
  #     resources :users, :only => [:index, :show] do
  #       resources :comments, :only => [:index]
  #     end
  #     resources :comments, :only => [:index]
  #     resources :featured_users, :only => [:index]
  #   end
  # end

  #
  # Catch other routes
  #
  root :to => 'home#index'
  get '/' => 'home#index', :as => :home
  
end
